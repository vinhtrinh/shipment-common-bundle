<?php
/**
 * Created by Rubikin Team.
 * Date: 5/19/14
 * Time: 7:49 PM
 * Question? Come to our website at http://rubikin.com
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Nilead\ShipmentCommonBundle\Message;


trait OrderRequestTrait
{
    protected $cod = 0;

    protected $note;

    protected $storeId;

    protected $orderId;

    protected $recipientId;

    protected $methodCode;

    public function setMethodCode($methodCode)
    {
        $this->methodCode = $methodCode;

        return $this;
    }

    public function getMethodCode()
    {
        return $this->methodCode;
    }

    public function setCOD($cod)
    {
        $this->cod = $cod;

        return $this;
    }

    public function getCOD()
    {
        return $this->cod;
    }

    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    public function getNote()
    {
        return $this->note;
    }

    public function setRecipientId($recipientId)
    {
        $this->recipientId = $recipientId;

        return $this;
    }

    public function getRecipientId()
    {
        return $this->recipientId;
    }

    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;

        return $this;
    }

    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * Sets shop id
     *
     * @param $storeId
     *
     * @return $this
     */
    public function setStoreId($storeId)
    {
        $this->storeId = $storeId;

        return $this;
    }

    /**
     * Gets storeId
     *
     * @return string
     */
    public function getStoreId()
    {
        return $this->storeId;
    }

}
